package com.vironit.rssbbc

import org.junit.Test

import org.junit.Assert.*

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class DateUnitTest {

    @Test
    fun parsingDate_isCorrect() {
        val longTime = "Mon, 05 Aug 2019 16:20:43 GMT".convertToLongTime()
        assertEquals(1565022043000, longTime)
    }

    @Test
    fun parsingDate_failed_correct(){
        val longTime = "not a time string".convertToLongTime()
        assertEquals(longTime, 0)
    }
}
