package com.vironit.rssbbc

import com.vironit.rssbbc.data.News
import com.vironit.rssbbc.data.RssParser
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import java.io.InputStream

private val testXmlString = "<rss><item>\n" +
        "            <title>\n" +
        "                <![CDATA[EU officials: No basis for further UK Brexit talks]]>\n" +
        "            </title>\n" +
        "            <description>\n" +
        "                <![CDATA[The bloc says it cannot accept any changes to the current withdrawal agreement to stop a \"no deal\".]]>\n" +
        "            </description>\n" +
        "            <link>https://www.bbc.co.uk/news/uk-politics-49240809</link>\n" +
        "            <guid isPermaLink=\"true\">https://www.bbc.co.uk/news/uk-politics-49240809</guid>\n" +
        "            <pubDate>Tue, 06 Aug 2019 06:58:46 GMT</pubDate>\n" +
        "        </item></rss>"

class RssParserTest {

    lateinit var rssParser: RssParser

    @Before
    fun instantiateRssParser() {
        rssParser = RssParser()
    }

    @Test
    fun parsingIsCorrect() {
        val news = rssParser.parse(testXmlString.byteInputStream() as InputStream)
        assertEquals(news.size, 1)
    }

}