package com.vironit.rssbbc.data

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.room.Room
import com.vironit.rssbbc.data.db.DbNews
import com.vironit.rssbbc.data.db.NewsDatabase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.xmlpull.v1.XmlPullParserException
import java.io.ByteArrayInputStream
import java.io.IOException
import java.io.InputStream
import java.net.HttpURLConnection
import java.net.URL

class Repository(private val context: Context): IRepository {

    companion object {
        private const val DB_NAME = "db_news"

        private const val URL_1 = "http://feeds.bbci.co.uk/news/rss.xml?edition=uk"
        private const val URL_2 = "http://feeds.bbci.co.uk/news/world/rss.xml?edition=uk"
        private const val URL_3 = "http://feeds.bbci.co.uk/news/england/rss.xml?edition=uk"
    }

    private val db: NewsDatabase =
        Room.databaseBuilder(context, NewsDatabase::class.java, DB_NAME).build()

    override fun getNewsFromLocalSource(): LiveData<List<DbNews>> {
        return db.newsDao().getNews()
    }

    override suspend fun markAllNewsAsRead() {
        withContext(Dispatchers.IO) {
            db.newsDao().markNewsAsRead()
        }
    }

    override suspend fun readNews(newsId: Long) {
        withContext(Dispatchers.IO) {
            db.newsDao().readNews(newsId)
        }
    }

    override suspend fun getNewsFromRemoteSource() {
        withContext(Dispatchers.IO) {
            val news1 = loadXmlFromNetwork(URL_1)
            val news2 = loadXmlFromNetwork(URL_2)
            val news3 = loadXmlFromNetwork(URL_3)
            val allNews = news1 + news2 + news3
            val dbNews = allNews.map { news ->
                DbNews(
                    news.title.hashCode().toLong(),
                    news.title,
                    news.description,
                    news.link,
                    news.date,
                    isRead = false,
                    isBlocked = false
                )
            }
            saveDbNews(dbNews)
        }
    }

    override suspend fun hideNews(id: Long) {
        withContext(Dispatchers.IO) {
            db.newsDao().hideNews(id)
        }
    }

    private fun saveDbNews(dbNews: List<DbNews>) {
        db.newsDao().saveNews(dbNews)
    }

    @Throws(XmlPullParserException::class, IOException::class)
    private fun loadXmlFromNetwork(urlString: String): List<News> {
        return downloadUrl(urlString)?.use { stream ->
            // Instantiate the parser
            RssParser().parse(ByteArrayInputStream(stream.readBytes()))
        } ?: emptyList()
    }

    @Throws(IOException::class)
    private fun downloadUrl(urlString: String): InputStream? {
        val url = URL(urlString)
        return (url.openConnection() as? HttpURLConnection)?.run {
            readTimeout = 10000
            connectTimeout = 15000
            requestMethod = "GET"
            doInput = true
            // Starts the query
            connect()
            inputStream
        }
    }

}