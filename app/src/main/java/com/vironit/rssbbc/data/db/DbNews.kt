package com.vironit.rssbbc.data.db

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "news")
data class DbNews(
    @PrimaryKey val id: Long,
    val title: String = "",
    val description: String = "",
    val link: String = "",
    val date: Long,
    var isRead: Boolean,
    var isBlocked: Boolean
)