package com.vironit.rssbbc.data.db

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface NewsDao {

    @Query("SELECT * FROM news WHERE isBlocked = 0 ORDER BY date DESC")
    fun getNews(): LiveData<List<DbNews>>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun saveNews(news: List<DbNews>)

    @Query("UPDATE news SET isRead = 1 WHERE id = :newsId")
    fun readNews(newsId: Long)

    @Query("UPDATE news SET isRead = 1 WHERE isRead = 0")
    fun markNewsAsRead()

    @Query("UPDATE news SET isBlocked = 1 WHERE id = :id")
    fun hideNews(id: Long)
}