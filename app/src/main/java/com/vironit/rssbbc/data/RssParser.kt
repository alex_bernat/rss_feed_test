package com.vironit.rssbbc.data

import android.util.Xml
import com.vironit.rssbbc.convertToLongTime
import org.xmlpull.v1.XmlPullParser
import org.xmlpull.v1.XmlPullParserException
import java.io.IOException
import java.io.InputStream

private val ns: String? = null

class RssParser {

    @Throws(XmlPullParserException::class, IOException::class)
    fun parse(inputStream: InputStream): List<News> {
        inputStream.use { stream ->
            val parser: XmlPullParser = Xml.newPullParser()
            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, true)
            parser.setInput(stream, null)
            parser.nextTag()
            return readFeed(parser)
        }
    }

    @Throws(XmlPullParserException::class, IOException::class)
    private fun readFeed(parser: XmlPullParser): List<News> {
        val news = mutableListOf<News>()

        parser.require(XmlPullParser.START_TAG, ns, "rss")
        parser.nextTag()
        parser.require(XmlPullParser.START_TAG, ns, "channel")
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.eventType != XmlPullParser.START_TAG) {
                continue
            }
            // Starts by looking for the entry tag
            if (parser.name == "item") {
                news.add(readNews(parser))
            } else {
                skip(parser)
            }
        }
        return news
    }

    @Throws(XmlPullParserException::class, IOException::class)
    private fun readNews(parser: XmlPullParser): News {
        parser.require(XmlPullParser.START_TAG, ns, "item")
        var title: String = ""
        var description: String = ""
        var link: String = ""
        var pubDate: Long = 0
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.eventType != XmlPullParser.START_TAG) {
                continue
            }
            when (parser.name) {
                "title" -> title = readText("title", parser)
                "description" -> description = readText("description", parser)
                "link" -> link = readText("link", parser)
                "pubDate" -> pubDate = readText("pubDate", parser).convertToLongTime()
                else -> skip(parser)
            }
        }
        return News(title, description, link, pubDate ?: 0)
    }

    @Throws(IOException::class, XmlPullParserException::class)
    private fun readText(tag: String, parser: XmlPullParser): String {
        parser.require(XmlPullParser.START_TAG, ns, tag)
        var result = ""
        if (parser.next() == XmlPullParser.TEXT) {
            result = parser.text
            parser.nextTag()
        }
        parser.require(XmlPullParser.END_TAG, ns, tag)
        return result
    }

    @Throws(XmlPullParserException::class, IOException::class)
    private fun skip(parser: XmlPullParser) {
        if (parser.eventType != XmlPullParser.START_TAG) {
            throw IllegalStateException()
        }
        var depth = 1
        while (depth != 0) {
            when (parser.next()) {
                XmlPullParser.END_TAG -> depth--
                XmlPullParser.START_TAG -> depth++
            }
        }
    }

}