package com.vironit.rssbbc.data

data class News(val title: String = "", val description: String = "", val link: String = "", val date: Long, var isRead: Boolean = false)