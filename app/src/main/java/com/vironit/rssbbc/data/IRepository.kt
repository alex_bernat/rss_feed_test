package com.vironit.rssbbc.data

import androidx.lifecycle.LiveData
import com.vironit.rssbbc.data.db.DbNews

interface IRepository {
    fun getNewsFromLocalSource(): LiveData<List<DbNews>>
    suspend fun markAllNewsAsRead()
    suspend fun readNews(newsId: Long)
    suspend fun getNewsFromRemoteSource()
    suspend fun hideNews(newsId: Long)
}