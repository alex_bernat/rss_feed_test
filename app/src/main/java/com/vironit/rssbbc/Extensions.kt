package com.vironit.rssbbc

import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

const val SERVER_PATTERN = "EEE, dd MMM yyyy HH:mm:ss z"
const val APP_PATTERN = "dd MMM yyyy HH:mm:ss"

fun String.convertToLongTime(): Long =
    try {
        SimpleDateFormat(SERVER_PATTERN).parse(this)?.time ?: 0
    } catch (e: ParseException) {
        0
    }

fun Long.convertToReadableString(): String {
    val date = Date(this)
    return try {
        SimpleDateFormat(APP_PATTERN, Locale.getDefault()).format(date)
    } catch (e: ParseException) {
        ""
    }
}