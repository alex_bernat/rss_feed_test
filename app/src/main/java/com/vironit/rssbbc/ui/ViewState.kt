package com.vironit.rssbbc.ui

sealed class ViewState
object Loading: ViewState()
data class Error(val message: String): ViewState()
object DataReady: ViewState()