package com.vironit.rssbbc.ui

import android.os.Bundle
import androidx.recyclerview.widget.DiffUtil
import com.vironit.rssbbc.data.db.DbNews

class NewsDiffUtil(
    private val oldNews: List<DbNews>,
    private val newNews: List<DbNews>
) : DiffUtil.Callback() {

    companion object {
        const val NEWS_READ = "NEWS_READ"
    }

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldNews[oldItemPosition].hashCode() == newNews[newItemPosition].hashCode()
    }

    override fun getOldListSize(): Int {
        return oldNews.size
    }

    override fun getNewListSize(): Int {
        return newNews.size
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldNews[oldItemPosition] == newNews[newItemPosition]
    }

    override fun getChangePayload(oldItemPosition: Int, newItemPosition: Int): Any? {
        val oldNews = oldNews[oldItemPosition]
        val newNews = newNews[newItemPosition]
        val diffBundle = Bundle()
        if (oldNews.isRead != newNews.isRead) {
            diffBundle.putInt(NEWS_READ, 0)
        }
        return diffBundle
    }
}