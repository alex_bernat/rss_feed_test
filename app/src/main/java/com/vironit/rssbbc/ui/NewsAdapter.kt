package com.vironit.rssbbc.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.vironit.rssbbc.R
import com.vironit.rssbbc.convertToReadableString
import com.vironit.rssbbc.data.db.DbNews

class NewsAdapter(val redirect: (DbNews) -> Unit, val hide: (Long) -> Unit) : RecyclerView.Adapter<NewsAdapter.NewsViewHolder>() {

    private var newsList = listOf<DbNews>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NewsViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.view_holder_news, parent, false)
        return NewsViewHolder(view)
    }

    override fun getItemCount(): Int {
        return newsList.size
    }

    override fun onBindViewHolder(holder: NewsViewHolder, position: Int) {
        holder.bind(newsList[position])
        holder.itemView.setOnClickListener {
            redirect(newsList[holder.adapterPosition])
        }
        holder.ivHide.setOnClickListener {
            hide(newsList[holder.adapterPosition].id)
        }
    }

    override fun onBindViewHolder(holder: NewsViewHolder, position: Int, payloads: MutableList<Any>) {
        if (payloads.isEmpty()) {
            onBindViewHolder(holder, position)
        } else {
            val diffBundle = payloads[0] as Bundle
            for (key in diffBundle.keySet()) {
                when (key) {
                    NewsDiffUtil.NEWS_READ ->
                        holder.setTitleColor(isRead = true)
                }
            }
        }
    }

    fun insertNews(newNews: List<DbNews>) {
        val diffResult = DiffUtil.calculateDiff(NewsDiffUtil(newsList, newNews))
        newsList = newNews
        diffResult.dispatchUpdatesTo(this)
    }

    inner class NewsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val tvTitle: TextView = itemView.findViewById(R.id.tv_title)
        private val tvDescription: TextView = itemView.findViewById(R.id.tv_description)
        private val tvDate: TextView = itemView.findViewById(R.id.tv_date)
        val ivHide: ImageView = itemView.findViewById(R.id.iv_hide)

        fun bind(news: DbNews) {
            tvTitle.text = news.title
            tvDescription.text = news.description
            tvDate.text = news.date.convertToReadableString()
            setTitleColor(news.isRead)
        }

        fun setTitleColor(isRead: Boolean) {
            tvTitle.apply {
                val resId = when (isRead) {
                    false -> R.color.color_not_read
                    true -> R.color.color_is_read
                }
                setTextColor(ContextCompat.getColor(context, resId))
            }
        }

    }
}