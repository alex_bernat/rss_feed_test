package com.vironit.rssbbc.ui

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.vironit.rssbbc.data.IRepository
import com.vironit.rssbbc.data.Repository
import com.vironit.rssbbc.data.db.DbNews
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import org.xmlpull.v1.XmlPullParserException
import java.io.IOException

class MainViewModel(application: Application) : AndroidViewModel(application) {

    private val repo: IRepository = Repository(getApplication())

    val news: LiveData<List<DbNews>> = repo.getNewsFromLocalSource()

    val state: LiveData<ViewState>
        get() = _state
    private val _state = MutableLiveData<ViewState>()

    init {
        loadNews()
    }

    fun readNews(newsId: Long) {
        viewModelScope.launch {
            repo.readNews(newsId)
        }
    }

    fun markAllNewsAsRead() {
        viewModelScope.launch {
            repo.markAllNewsAsRead()
        }
    }

    fun loadNews() {
        viewModelScope.launch {
            _state.value = Loading
            try {
                repo.getNewsFromRemoteSource()
                _state.postValue(DataReady)
            } catch (e: XmlPullParserException) {
                _state.postValue(Error("Xml error"))
            } catch (e: IOException) {
                _state.postValue(Error("Connection error"))
            }
        }
    }

    fun hideNews(id: Long) {
        viewModelScope.launch {
            repo.hideNews(id)
        }
    }

}