package com.vironit.rssbbc.ui

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.google.android.material.snackbar.Snackbar
import com.vironit.rssbbc.R
import com.vironit.rssbbc.data.db.DbNews

class MainActivity : AppCompatActivity() {

    private lateinit var viewModel: MainViewModel

    private lateinit var refresh: SwipeRefreshLayout
    private lateinit var rvNews: RecyclerView
    private lateinit var adapterNews: NewsAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initRefresh()
        initNewsRv()

        viewModel = ViewModelProviders.of(this).get(MainViewModel::class.java)
        viewModel.news.observe(this, Observer {
            adapterNews.insertNews(it)
        })
        viewModel.state.observe(this, Observer {
            when (it) {
                is Loading -> showLoading()
                is DataReady -> hideLoading()
                is Error -> {
                    hideLoading()
                    showMessage(refresh, it.message)
                }
            }
        })
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.menu_read -> {
                markAllNewsRead()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun initRefresh() {
        refresh = findViewById(R.id.refresh)
        refresh.setOnRefreshListener {
            viewModel.loadNews()
        }
    }

    private fun initNewsRv() {
        rvNews = findViewById(R.id.rv_news)
        rvNews.layoutManager = LinearLayoutManager(this)
        adapterNews = NewsAdapter({
            readNews(it)
        }, {
            hideNews(it)
        })
        rvNews.adapter = adapterNews
    }

    private fun readNews(news: DbNews) {
        viewModel.readNews(news.id)
        Intent(Intent.ACTION_VIEW).apply {
            data = Uri.parse(news.link)
            if (resolveActivity(packageManager) != null) {
                startActivity(this)
            } else {
                showMessage(refresh, "Sorry, you have no apps that can perform an action.")
            }
        }
    }

    private fun hideNews(newsId: Long) {
        viewModel.hideNews(newsId)
    }


    private fun markAllNewsRead() {
        viewModel.markAllNewsAsRead()
    }

    private fun showMessage(rootView: View, message: String) {
        Snackbar.make(rootView, message, Snackbar.LENGTH_LONG).show()
    }

    private fun showLoading() {
        refresh.isRefreshing = true
    }

    private fun hideLoading() {
        refresh.isRefreshing = false
    }

}
